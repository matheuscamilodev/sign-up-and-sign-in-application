# Projeto Website Sign In e Sign Up

## Resumo do projeto
Aplicação FullStack com as páginas: 
- Home (Página Inicial)
- Login
- Cadastro de usuários
- Lista de usuários cadastrados
&nbsp;

## Funcionalidades:
- Login
  - Verificação de email e senha no banco de dados
  - Senha guardada no banco de dados criptografada em sha256

- Cadastro de Usuários
  - Verificação de existência de email já registrado

- Página de administração de usuários
  - Apenas acessada se usuário estiver logado

- Logout
&nbsp;

## Como configurar aplicação?
Existem dois meios de executar a aplicação:
- Usando o python instalado no sistema físico
- Usando uma imagem do python em um container, neste caso o docker 

### Usando o python instalado
[Url para download do python, baixar versão 3.8](https://www.python.org/downloads/)

1. Criar e iniciar ambiente virtual python

 a. Linux
```sh
python3 -m venv venv
source venv/bin/activate 
```

 b. Windows
```shell
python -m venv venv
.\venv\Scripts\activate 
```

2. Instalar os requisitos do projeto
```sh
pip install -r requirements.txt
```

3. Rodar a aplicação
```sh
flask run
```

### Usando o docker
[Url para download do docker](https://docs.docker.com/get-docker/)

1. Criar e iniciar o container

a. Linux
```sh
sudo docker run -it -p 5000:5000  $(sudo docker build -q -t user_app .)
```

b. Windows (administrador)
```sh
docker build -t user_app . 
docker run -it -p 5000:5000 user_app
```

E pronto, agora é so acessar a url da [ Página Inicial ](http://localhost:5000/) (http://localhost:5000/)   
&nbsp;

## Urls do website
|Requisição | Endpoint        | Breve descrição                     |
|-----------|-----------------|-------------------------------------|
| GET       | /signup         | Página de Cadastro de Usuários      |
| POST      | /signup         | Envio dados do Cadastro de Usuários |
| GET       | /signin         | Página de Login                     |
| POST      | /signin         | Envio de credenciais de Login       |
| GET       | /signout        | Usuário deslogado da aplicação      |
| GET       | /users (logado) | Lista de usuários registrados       |
&nbsp;

## Informações técnicas
### Linguagem de programação usada: Python 3.8
### Frameworks e bibliotecas python usadas:
```txt
flask:latest
pytest:latest
```
### Outros frameworks:
```txt
Bootstrap (css):5.1.3
```