import hashlib


def encrypt_sha256(hash_string: str):
    return hashlib.sha256(hash_string.encode()).hexdigest()
