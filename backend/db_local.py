import os
from json import dumps, loads


class DBLocal:
    def __init__(self, field):
        self.path = './db/db_local.json'
        self.abs_path = os.path.abspath(self.path)
        self.field = field

    def get_data_from_local_db(self):
        with open(self.path, 'r') as file:
            data = loads(file.read())
        return data

    def set_data_from_local_db(self, data: dict):
        with open(self.path, 'w') as file:
            file.write(dumps(data))

    def add_data_from_local_db(self, data: dict):
        new_data = self.get_data_from_local_db()
        new_data[self.field].append(data)
        with open(self.path, 'w') as file:
            file.write(dumps(new_data))


if __name__ == "__main__":
    DBLocal('users')
