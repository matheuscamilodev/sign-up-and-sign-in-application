from backend.db_local import DBLocal
from backend.utils import encrypt_sha256


class User:
    def __init__(self):
        self.db_local = DBLocal('users')
        self.user_data = {}
        self.sign_up_fields = [
            'name', 'email', 'birth_date', 'cpf',
            'phone_number', 'password']
        self.sign_in_fields = ['email', 'password']

    def sign_in(self, login_credentials: dict):
        if self.__check_fields(login_credentials, self.sign_in_fields):
            self.user_data = self.__check_login(login_credentials)
        return self.user_data != {}

    def sign_up(self, user_data: dict):
        if all([
            self.__check_fields(user_data, self.sign_up_fields),
            not self.__check_if_exists(user_data)
        ]):
            user_data['password'] = encrypt_sha256(user_data['password'])
            self.db_local.add_data_from_local_db(user_data)
            return True
        return False

    def sign_out(self):
        self.user_data = {}

    def clean_up_records(self):
        self.db_local.set_data_from_local_db({'users': []})

    def get_users_list(self):
        return self.db_local.get_data_from_local_db().get('users', {})

    def __check_login(self, login: dict):
        db_local_data = self.db_local.get_data_from_local_db()
        for user in db_local_data.get('users', []):
            if all([
                user['email'] == login['email'],
                user['password'] == encrypt_sha256(login['password'])
            ]):
                return user
        return {}

    def __check_fields(self, data: dict, fields: list):
        return sorted(list(data.keys())) == sorted(fields)

    def __check_if_exists(self, data: dict):
        db_local_data = self.db_local.get_data_from_local_db()
        if db_local_data.get('users', []):
            for user in db_local_data.get('users'):
                if data['email'] == user['email']:
                    return True
        return False
