from backend.user import User
from backend.core import context

from jinja2 import FileSystemLoader
from flask import Flask, render_template, request, redirect

app = Flask(__name__)
app.jinja_loader = FileSystemLoader('frontend/templates')

user = User()


@app.route('/')
def home_page():
    if user.user_data:
        context.update({'logged_user': user.user_data})
    return render_template('home.html', **context)


@app.route('/signup', methods=['GET', 'POST'])
def sign_up_page():
    if user.user_data:
        return redirect('/')

    context.pop('msg', None)
    if request.method == 'POST':
        user_data = request.form.to_dict()
        if user.sign_up(user_data):
            return redirect('/signin')
        context.update({'msg': 'User with this email already exists!'})
    context.update({'signup_fields': user.sign_up_fields})
    return render_template('signup.html', **context)


@app.route('/signin', methods=['GET', 'POST'])
def sign_in_page():
    if user.user_data:
        return redirect('/')

    context.pop('msg', None)
    context.update({'signin_fields': user.sign_in_fields})
    if request.method == 'POST':
        login_credentials = request.form
        if user.sign_in(login_credentials):
            return redirect('/')
        context.update({'msg': 'Incorrect Email or Password!'})
    return render_template('signin.html', **context)


@app.route('/signout')
def sign_out_user():
    user.sign_out()
    context.pop('logged_user', None)
    return redirect('/')


@app.route('/users')
def list_all_users():
    if user.user_data:
        users_list = user.get_users_list()
        context.update({'users_list': users_list})
        return render_template('users_list.html', **context)
    return redirect('/')


if __name__ == "__main__":
    app.run()
