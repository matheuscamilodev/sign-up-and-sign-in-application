import os
import sys

import pytest

sys.path.insert(1, os.path.abspath('.'))

from backend.user import User  # noqa: E402


@pytest.fixture
def user_initialized():
    user = User()
    user.clean_up_records()
    return user


@pytest.fixture
def user_data():
    return {
        'name': 'usuario',
        'email': 'abc@gmail.com',
        'password': '12345',
        'birth_date': '23/06/2000',
        'cpf': '000.000.000-00',
        'phone_number': '(00) 0000-0000'
    }


def test_user_sign_up_success(user_initialized, user_data):
    assert user_initialized.sign_up(user_data) is True


def test_user_sign_up_alread_exists(user_initialized, user_data):
    user_initialized.sign_up(user_data)
    assert user_initialized.sign_up(user_data) is False


def test_user_sign_up_missing_field(user_initialized, user_data):
    user_data.pop('password')
    assert user_initialized.sign_up(user_data) is False


def test_user_sign_in_success(user_initialized, user_data):
    user_initialized.sign_up(user_data)
    login_credentials = {
        'email': 'abc@gmail.com',
        'password': '12345',
    }
    assert user_initialized.sign_in(login_credentials) is True


def test_user_sign_in_missing_field(user_initialized, user_data):
    user_initialized.sign_up(user_data)
    login_credentials = {
        'email': 'abc@gmail.com',
    }
    assert user_initialized.sign_up(login_credentials) is False


def test_user_sign_in_not_found_login(user_initialized, user_data):
    user_initialized.sign_up(user_data)
    login_credentials = {
        'email': 'abc@gmail.com',
        'password': 'incorrext_password',
    }
    assert user_initialized.sign_up(login_credentials) is False
